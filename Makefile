TARGET=target/riscv64gc-unknown-none-elf/debug/myos

QEMU_ARGS := -machine virt \
        -nographic \
        -bios ../../bootloader/rustsbi-qemu.bin \
        -device loader,file=target/riscv64gc-unknown-none-elf/debug/myos.bin,addr=0x80200000

obj:
	rust-objcopy --strip-all target/riscv64gc-unknown-none-elf/debug/myos -O binary target/riscv64gc-unknown-none-elf/debug/myos.bin

stat: obj
	stat target/riscv64gc-unknown-none-elf/debug/myos
	stat target/riscv64gc-unknown-none-elf/debug/myos.bin

run: obj
	@qemu-system-riscv64 $(QEMU_ARGS)

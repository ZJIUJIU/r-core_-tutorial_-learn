// 移除标准库
#![no_std]
// 移除入口函数
#![no_main]

// 声明panic mod
mod lang_items;

use core::arch::global_asm;

// asm 加载第一行指令
global_asm!(include_str!("entry.asm"));